#!/bin/bash

# Random simulation:
# spin -c cube.pml
# spin -p cube.pml

rm *trail *rst
rm *.tcl
mv result{,.bak}

# sudo /sbin/sysctl kernel.shmmax=32212254720
# sudo /sbin/sysctl kernel.shmall=7864320

version=simple
# version=""

# Monothread, DFS, local:
spin -a -b cube$version.pml
gcc -O2 -DSAFETY -DBITSTATE -DMEMLIM=14000 -o pan pan.c
./pan -v -n -E -m50000 -w30 cube$version.pml |& tee -a result

# Monothread, DFS, node:
# spin -a -b -v cube$version.pml
# gcc -O2 -DSAFETY -DBITSTATE -DMEMLIM=120000 -o pan pan.c
# ./pan -v -n -E -m5100 -w39 cube$version.pml |& tee -a result

# Multithread, DFS, node:
# doclean
# spin -a -b -v cube$version.pml
# gcc -O2 -DSAFETY -DBITSTATE -DMEMLIM=120000 -DUSE_DISK -DMAX_DSK_FILE=10000000 -DNCORE=10 -DVMAX=264 -DFULL_TRAIL -o pan pan.c
# gcc -O2 -DSAFETY -DBITSTATE -DMEMLIM=120000 -DSEP_STATE -DNCORE=10 -DVMAX=264 -DFULL_TRAIL -o pan pan.c
# gcc -O2 -DSAFETY -DBITSTATE -DMEMLIM=120000 -DSET_WQ_SIZE=2000 -DNCORE=5 -DVMAX=264 -DFULL_TRAIL -o pan pan.c
# gcc -O2 -DSAFETY -DBITSTATE -DMEMLIM=120000 -DNCORE=50 -DVMAX=264 -o pan pan.c
# ./pan -v -n -E -w33 -m5100 -z50 |& tee -a result

# Multithread, BFS, node:
# doclean
# spin -a -b -v cube$version.pml
# gcc -O2 -DBFS_PAR -DSAFETY -DBITSTATE -DMEMLIM=100000 -o pan pan.c
# ./pan -v -n -E -w36 -u50 -m50000 |& tee -a result

# The handoff determines the length of the counter-example !!
# We cannot set it too large because then only one core works.
# So use DFULL_TRAIL.

echo "Going to replay error trail" |& tee -a result
./pan -r |& tee -a result
