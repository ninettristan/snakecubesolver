/* Casse-tête of Aude */
/* lib */

inline buildchain()
{
	i = 1;
	do
	:: i < 27 ->

		chain[i].x = chain[i-1].x;
		chain[i].y = chain[i-1].y;
		chain[i].z = chain[i-1].z;

		if
		:: chain[i].dir == 1 || chain[i].dir == -1 -> chain[i].x = chain[i-1].x + chain[i].dir
		:: chain[i].dir == 2 || chain[i].dir == -2 -> chain[i].y = chain[i-1].y + chain[i].dir / 2
		:: chain[i].dir == 3 || chain[i].dir == -3 -> chain[i].z = chain[i-1].z + chain[i].dir / 3
		fi;

		i++
	:: else -> break
	od
}

inline checkoutrun()
{
	atomic {
		/* int d1, d2, d3 = 0; */
		int min1, max1, min2, max2, min3, max3 = 0;
		bool outrun = false;

		i = 1;
		do
		:: i < 27 ->
			/* if */
			/* :: chain[i].dir == 1 || chain[i].dir == -1 -> d1 = d1 + chain[i].dir */
			/* :: chain[i].dir == 2 || chain[i].dir == -2 -> d2 = d2 + chain[i].dir / 2 */
			/* :: chain[i].dir == 3 || chain[i].dir == -3 -> d3 = d3 + chain[i].dir / 3 */
			/* fi; */

			/* if */
			/* :: d1 < min1 -> min1 = d1 */
			/* :: d1 > max1 -> max1 = d1 */
			/* :: d2 < min2 -> min2 = d2 */
			/* :: d2 > max2 -> max2 = d2 */
			/* :: d3 < min3 -> min3 = d3 */
			/* :: d3 > max3 -> max3 = d3 */
			/* :: else -> skip */
			/* fi; */

			if
			:: chain[i].x < min1 -> min1 = chain[i].x
			:: chain[i].x > max1 -> max1 = chain[i].x
			:: else -> skip
			fi;

			if
			:: chain[i].y < min2 -> min2 = chain[i].y
			:: chain[i].y > max2 -> max2 = chain[i].y
			:: else -> skip
			fi;

			if
			:: chain[i].z < min3 -> min3 = chain[i].z
			:: chain[i].z > max3 -> max3 = chain[i].z
			:: else -> skip
			fi;

			if
			/* :: max1 - min1 > 2 || max2 - min2 > 2 || max3 - min3 > 2 -> outrun = true; break */
			:: max1 - min1 > 3 || max2 - min2 > 3 || max3 - min3 > 3 -> outrun = true; break
			/* :: max1 - min1 > 4 || max2 - min2 > 4 || max3 - min3 > 4 -> outrun = true; break */
			:: else -> skip
			fi;

			i++
		:: else -> break
		od;

		assert(outrun)

	}
}

inline tryrot(pivot)
{
	atomic {
		countrottriesglobal++;
		overlap = false;

		/* newchain = chain; */
		/* copychain(true, 0); */

		i = pivot;
		do
		:: i < 27 ->
			newchain[i].dir = chain[i].dir;
			newchain[i].x = chain[i].x;
			newchain[i].y = chain[i].y;
			newchain[i].z = chain[i].z;
			i++
		:: else -> break
		od;

		select (ang : 1..3);

		i = pivot + 1;
		do
		:: i < 27 ->

			j = 0;
			do
			:: j < ang ->
				if
				:: newchain[pivot].dir == 1 || newchain[pivot].dir == -1 ->
					if
					:: newchain[i].dir == -3 -> newchain[i].dir = -2
					:: newchain[i].dir == -2 -> newchain[i].dir = 3
					:: newchain[i].dir == 2 -> newchain[i].dir = -3
					:: newchain[i].dir == 3 -> newchain[i].dir = 2
					:: else -> skip
					fi
				:: newchain[pivot].dir == 2 || newchain[pivot].dir == -2 ->
					if
					:: newchain[i].dir == -3 -> newchain[i].dir = 1
					:: newchain[i].dir == -1 -> newchain[i].dir = -3
					:: newchain[i].dir == 1 -> newchain[i].dir = 3
					:: newchain[i].dir == 3 -> newchain[i].dir = -1
					:: else -> skip
					fi
				:: newchain[pivot].dir == 3 || newchain[pivot].dir == -3 ->
					if
					:: newchain[i].dir == -2 -> newchain[i].dir = 1
					:: newchain[i].dir == -1 -> newchain[i].dir = -2
					:: newchain[i].dir == 1 -> newchain[i].dir = 2
					:: newchain[i].dir == 2 -> newchain[i].dir = -1
					:: else -> skip
					fi
				fi;
				j++

			:: else -> break
			od;

			newchain[i].x = newchain[i-1].x;
			newchain[i].y = newchain[i-1].y;
			newchain[i].z = newchain[i-1].z;

			if
			:: newchain[i].dir == 1 || newchain[i].dir == -1 -> newchain[i].x = newchain[i-1].x + newchain[i].dir
			:: newchain[i].dir == 2 || newchain[i].dir == -2 -> newchain[i].y = newchain[i-1].y + newchain[i].dir / 2
			:: newchain[i].dir == 3 || newchain[i].dir == -3 -> newchain[i].z = newchain[i-1].z + newchain[i].dir / 3
			fi;

			j = 0;
			do
			:: j < pivot + 1 ->
				if
				:: newchain[i].x == chain[j].x && \
					newchain[i].y == chain[j].y && \
					newchain[i].z == chain[j].z ->
					overlap = true;
					break

				:: else -> skip
				fi;
				j++

			:: pivot < j && j < i ->
				if
				:: newchain[i].x == newchain[j].x && \
					newchain[i].y == newchain[j].y && \
					newchain[i].z == newchain[j].z ->
					overlap = true;
					break

				:: else -> skip
				fi;
				j++

			:: else -> break
			od;

			if
			:: overlap -> break
			:: else -> skip
			fi;

			/* if */
			/* :: newchain[i].x > maxx */

			i++

		:: else -> break
		od;

		/* checkoverlap(overlap); */
		/* check(outrun, overlap); */

		
		if
		:: overlap -> goto Choice
		:: else -> skip
		fi;

		/* chain = newchain; */
		/* copychain(newchain, chain, pivot + 1); */

		i = pivot + 1;
		do
		:: i < 27 ->
			chain[i].dir = newchain[i].dir;
			chain[i].x = newchain[i].x;
			chain[i].y = newchain[i].y;
			chain[i].z = newchain[i].z;
			i++
		:: else -> break
		od;

		countrotglobal++;
		countrotlocal[pivot] = true;

		checkoutrun()
	}
}
