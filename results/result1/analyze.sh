#!/bin/bash

# Random simulation:
# spin -c cube.pml
# spin -p cube.pml

rm *trail *rst
rm *.tcl
mv result{,.bak}

# sudo /sbin/sysctl kernel.shmmax=32212254720
# sudo /sbin/sysctl kernel.shmall=7864320

version=simple
# version=""

# Monothread, safety:
# spin -b -run -O2 -DSAFETY -DBITSTATE -DMEMLIM=100000 -v -n -E -w39 -m30000 cube$version.pml |& tee -a result
# spin -b -run -O2 -DVECTORSZ=1700 -DSAFETY -DBITSTATE -DMEMLIM=14000 -v -n -m100 -w36 cube$version.pml |& tee -a result
# spin -b -run -O2 -DSAFETY -DBITSTATE -DMEMLIM=14000 -v -n -E -m30000 -w36 cube$version.pml |& tee -a result

# spin -a -b cube$version.pml
# gcc -O2 -DSAFETY -DBITSTATE -DMEMLIM=14000 -o pan pan.c
# ./pan -v -n -E -m50000 -w36 cube$version.pml |& tee -a result
# echo "Going to replay error trail" |& tee -a result
# ./pan -r |& tee -a result
# exit

doclean

# Multithread, DFS:
# spin -b -run -O2 -DSAFETY -DBITSTATE -DMEMLIM=100000 -DNCORE=50 -DVMAX=320 -v -n -w36 \
# 	-m30000 -z50 cube$version.pml |& tee -a result
# spin -b -run -O2 -DSAFETY -DBITSTATE -DMEMLIM=100000 -DNCORE=10 -v -n -E -w39 \
# 	-m30000 -z50 cube$version.pml |& tee -a result

spin -a -b -v cube$version.pml
# gcc -O2 -DSAFETY -DBITSTATE -DMEMLIM=120000 -DNCORE=50 -DVMAX=264 -DFULL_TRAIL -o pan pan.c
gcc -O2 -DSAFETY -DBITSTATE -DMEMLIM=120000 -DNCORE=50 -DVMAX=264 -o pan pan.c
# ./pan -v -n -E -w39 -Q1 -m50000 |& tee -a result
./pan -v -n -E -w39 -m5000 -z50 |& tee -a result

# Multithread, BFS:
# spin -a -b -v cube$version.pml
# gcc -O2 -DBFS_PAR -DSAFETY -DBITSTATE -DMEMLIM=100000 -o pan pan.c
# ./pan -v -n -E -w36 -u50 -m50000 |& tee -a result

# The handoff determines the length of the counter-example !!
# We cannot set it too large because then only one core works.
# So use DFULL_TRAIL.

echo "Going to replay error trail" |& tee -a result
./pan -r |& tee -a result
