/* Cube casse-tête library */

inline updateCoord(i)
{
	chain[i].x = chain[i-1].x;
	chain[i].y = chain[i-1].y;
	chain[i].z = chain[i-1].z;

	if
	:: chain[i].dir == 1 || chain[i].dir == -1 -> chain[i].x = chain[i-1].x + chain[i].dir
	:: chain[i].dir == 2 || chain[i].dir == -2 -> chain[i].y = chain[i-1].y + chain[i].dir / 2
	:: chain[i].dir == 3 || chain[i].dir == -3 -> chain[i].z = chain[i-1].z + chain[i].dir / 3
	fi
}

inline checkOutrun()
{
	skip; /* see http://spinroot.com/spin/Man/d_step.html */

	d_step {
		short minx, maxx, miny, maxy, minz, maxz = 0;
		unsigned i : 5;
		outrun = false;

		for (i : 1 .. ChainSize - 1) {
			if
			:: chain[i].x < minx -> minx = chain[i].x
			:: chain[i].x > maxx -> maxx = chain[i].x
			:: else -> skip
			fi;

			if
			:: chain[i].y < miny -> miny = chain[i].y
			:: chain[i].y > maxy -> maxy = chain[i].y
			:: else -> skip
			fi;

			if
			:: chain[i].z < minz -> minz = chain[i].z
			:: chain[i].z > maxz -> maxz = chain[i].z
			:: else -> skip
			fi;

			if
			:: maxx - minx > CubeSize - 1 || maxy - miny > CubeSize - 1 || maxz - minz > CubeSize - 1 -> outrun = true; break
			:: else -> skip
			fi
		};

		skip /* see http://spinroot.com/spin/Man/d_step.html */
	}
}

inline checkOverlap()
{
	skip; /* see http://spinroot.com/spin/Man/d_step.html */

	d_step
	{
		unsigned i : 5;
		unsigned j : 5;
		overlap = false;

		for (i : 1 .. ChainSize - 1) {
			for (j : 0 .. i - 1) {
				if
				:: chain[i].x == chain[j].x &&
					chain[i].y == chain[j].y &&
					chain[i].z == chain[j].z ->
					overlap = true;
					break
				:: else -> skip
				fi
			};

			if
			:: overlap -> break
			:: else -> skip
			fi
		};

		skip /* see http://spinroot.com/spin/Man/d_step.html */
	}
}

inline rotate(pivot)
{
	skip; /* see http://spinroot.com/spin/Man/d_step.html */

	d_step
	{
		unsigned i : 5;

		for (i : pivot + 1 .. ChainSize - 1) {
			if
			:: chain[pivot].dir == 1 || chain[pivot].dir == -1 ->
				if
				:: chain[i].dir == -3 -> chain[i].dir = -2
				:: chain[i].dir == -2 -> chain[i].dir = 3
				:: chain[i].dir == 2 -> chain[i].dir = -3
				:: chain[i].dir == 3 -> chain[i].dir = 2
				:: else -> skip
				fi
			:: chain[pivot].dir == 2 || chain[pivot].dir == -2 ->
				if
				:: chain[i].dir == -3 -> chain[i].dir = 1
				:: chain[i].dir == -1 -> chain[i].dir = -3
				:: chain[i].dir == 1 -> chain[i].dir = 3
				:: chain[i].dir == 3 -> chain[i].dir = -1
				:: else -> skip
				fi
			:: chain[pivot].dir == 3 || chain[pivot].dir == -3 ->
				if
				:: chain[i].dir == -2 -> chain[i].dir = 1
				:: chain[i].dir == -1 -> chain[i].dir = -2
				:: chain[i].dir == 1 -> chain[i].dir = 2
				:: chain[i].dir == 2 -> chain[i].dir = -1
				:: else -> skip
				fi
			fi;

			updateCoord(i)
		};

		skip /* see http://spinroot.com/spin/Man/d_step.html */
	}
}
