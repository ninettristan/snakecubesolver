/* Cube casse-tête */

/* Throws a warning because ChainSize needs to be read-only, */
/* as it is used as an array size. */
/* byte ChainSize 27; */

# define ChainSize 27
# define CubeSize 3

typedef Cube {
	short x;
	short y;
	short z;
	short dir
};

Cube chain[ChainSize];
/* hidden byte localRotationCount[ChainSize]; */
/* hidden byte globalRotationCount = 0; */
/* hidden byte globalRotationTriesCount = 0; */
byte localRotationCount[ChainSize];
byte globalRotationCount = 0;
byte globalRotationTriesCount = 0;
bool overlap, outrun;

# include "lib.pml"

proctype Solve()
{
	/* byte pivot; */
	byte pivot, angle;
	/* bool overlap, outrun; */

again:
	skip; /* see d_step in http://spinroot.com/spin/Man/Quick.html */
end:
	/* select(pivot : 1 .. ChainSize - 2); */

	/* if */
	/* :: chain[pivot].dir == chain[pivot + 1].dir || */
	/* 	localRotationCount >= 3 -> */
	/* 	goto again */
	/* :: else -> skip */
	/* fi; */

	/* chain[i] is a pivot if chain[i+1] exists and chain[i].dir != chain[i+1].dir */
	if
	:: localRotationCount[24] < 3 -> pivot = 24
	:: localRotationCount[23] < 3 -> pivot = 23
	:: localRotationCount[22] < 3 -> pivot = 22
	:: localRotationCount[20] < 3 -> pivot = 20
	:: localRotationCount[19] < 3 -> pivot = 19
	:: localRotationCount[17] < 3 -> pivot = 17
	:: localRotationCount[16] < 3 -> pivot = 16
	:: localRotationCount[15] < 3 -> pivot = 15
	:: localRotationCount[13] < 3 -> pivot = 13
	:: localRotationCount[11] < 3 -> pivot = 11
	:: localRotationCount[10] < 3 -> pivot = 10
	:: localRotationCount[9] < 3 -> pivot = 9
	:: localRotationCount[8] < 3 -> pivot = 8
	:: localRotationCount[6] < 3 -> pivot = 6
	:: localRotationCount[4] < 3 -> pivot = 4
	fi;

	/* rotate(pivot, 1); */
	/* globalRotationCount++; */
	/* localRotationCount[pivot]++; */

	select(angle : 1 .. 3 - localRotationCount[pivot]);
	rotate(pivot, angle);
	globalRotationTriesCount++;
	checkOverlap(pivot);

	if
	/* :: overlap -> goto again */
	:: overlap ->
		rotate(pivot, (4 - angle));
		goto again
	:: else -> skip
	fi;

	globalRotationCount++;
	localRotationCount[pivot] = localRotationCount[pivot] + angle;

	checkOutrun();
	assert(outrun);
	goto again
}

init
{
	skip; /* see http://spinroot.com/spin/Man/d_step.html */

	d_step
	{
		byte i;

		chain[1].dir = 3;
		chain[2].dir = 3;
		chain[3].dir = 1;
		chain[4].dir = 1;
		chain[5].dir = 3;
		chain[6].dir = 3;
		chain[7].dir = 1;
		chain[8].dir = 1;
		chain[9].dir = 3;
		chain[10].dir = 1;
		chain[11].dir = 3;
		chain[12].dir = 1;
		chain[13].dir = 1;
		chain[14].dir = 3;
		chain[15].dir = 3;
		chain[16].dir = 1;
		chain[17].dir = 3;
		chain[18].dir = 1;
		chain[19].dir = 1;
		chain[20].dir = 3;
		chain[21].dir = 1;
		chain[22].dir = 1;
		chain[23].dir = 3;
		chain[24].dir = 1;
		chain[25].dir = 3;
		chain[26].dir = 3;

		for (i : 1 .. ChainSize - 1) {
			updateCoord(i)
		};

		skip /* see http://spinroot.com/spin/Man/d_step.html */
	};

	run Solve()
}
