/* Casse-tête of Aude */

typedef Cube {
	int x;
	int y;
	int z;
	int dir
}

Cube chain[27];
Cube newchain[27];
int countrotlocal[27];
int countrotglobal = 0;
int countrottriesglobal = 0;
int ang;

# include "libcomplex.pml"

proctype Solve()
{
	bool overlap;
	int i, j;

Choice:
	/* chain[i] is a pivot if chain[i+1] exists and chain[i] != chain[i+1] */
	do
	:: !countrotlocal[24] -> tryrot(24)
	:: !countrotlocal[23] -> tryrot(23)
	:: !countrotlocal[22] -> tryrot(22)
	:: !countrotlocal[20] -> tryrot(20)
	:: !countrotlocal[19] -> tryrot(19)
	:: !countrotlocal[17] -> tryrot(17)
	:: !countrotlocal[16] -> tryrot(16)
	:: !countrotlocal[15] -> tryrot(15)
	:: !countrotlocal[13] -> tryrot(13)
	:: !countrotlocal[11] -> tryrot(11)
	:: !countrotlocal[10] -> tryrot(10)
	:: !countrotlocal[9] -> tryrot(9)
	:: !countrotlocal[8] -> tryrot(8)
	:: !countrotlocal[6] -> tryrot(6)
	:: !countrotlocal[4] -> tryrot(4)
	od
}

init {
	atomic {
		int i;

		chain[1].dir = 3;
		chain[2].dir = 3;
		chain[3].dir = 1;
		chain[4].dir = 1;
		chain[5].dir = 3;
		chain[6].dir = 3;
		chain[7].dir = 1;
		chain[8].dir = 1;
		chain[9].dir = 3;
		chain[10].dir = 1;
		chain[11].dir = 3;
		chain[12].dir = 1;
		chain[13].dir = 1;
		chain[14].dir = 3;
		chain[15].dir = 3;
		chain[16].dir = 1;
		chain[17].dir = 3;
		chain[18].dir = 1;
		chain[19].dir = 1;
		chain[20].dir = 3;
		chain[21].dir = 1;
		chain[22].dir = 1;
		chain[23].dir = 3;
		chain[24].dir = 1;
		chain[25].dir = 3;
		chain[26].dir = 3;

		buildchain();

		run Solve()
	}
}
