/* Cube casse-tête */

/* Throws a warning because ChainSize needs to be read-only, */
/* as it is used as an array size. */
/* byte ChainSize 27; */

# define ChainSize 27
# define CubeSize 3

typedef Cube {
	short x;
	short y;
	short z;
	short dir
};

Cube chain[ChainSize];
/* int globalRotationTriesCount = 0; */

# include "libsimple.pml"

proctype Solve()
{
	bool overlap, outrun;

again:
	skip; /* see d_step in http://spinroot.com/spin/Man/Quick.html */
/* end: */
	/* select(pivot : 1 .. ChainSize - 2); */

	/* if */
	/* :: chain[pivot].dir == chain[pivot + 1].dir || */
	/* 	localRotationCount >= 3 -> */
	/* 	goto again */
	/* :: else -> skip */
	/* fi; */

	/* chain[i] is a pivot if chain[i+1] exists and chain[i].dir != chain[i+1].dir */
	if
	:: rotate(24)
	:: rotate(23)
	:: rotate(22)
	:: rotate(20)
	:: rotate(19)
	:: rotate(17)
	:: rotate(16)
	:: rotate(15)
	:: rotate(13)
	:: rotate(11)
	:: rotate(10)
	:: rotate(9)
	:: rotate(8)
	:: rotate(6)
	:: rotate(4)
	:: rotate(2)
	fi;

	/* globalRotationTriesCount++; */

	checkOverlap();

	if
	:: overlap -> goto again
	:: else -> skip
	fi;

	checkOutrun();
	assert(outrun);
	goto again
}

init
{
	skip; /* see http://spinroot.com/spin/Man/d_step.html */

	d_step
	{
		unsigned i : 5;

		chain[1].dir = 3;
		chain[2].dir = 3;
		chain[3].dir = 1;
		chain[4].dir = 1;
		chain[5].dir = 3;
		chain[6].dir = 3;
		chain[7].dir = 1;
		chain[8].dir = 1;
		chain[9].dir = 3;
		chain[10].dir = 1;
		chain[11].dir = 3;
		chain[12].dir = 1;
		chain[13].dir = 1;
		chain[14].dir = 3;
		chain[15].dir = 3;
		chain[16].dir = 1;
		chain[17].dir = 3;
		chain[18].dir = 1;
		chain[19].dir = 1;
		chain[20].dir = 3;
		chain[21].dir = 1;
		chain[22].dir = 1;
		chain[23].dir = 3;
		chain[24].dir = 1;
		chain[25].dir = 3;
		chain[26].dir = 3;

		for (i : 1 .. ChainSize - 1) {
			updateCoord(i)
		};

		skip /* see http://spinroot.com/spin/Man/d_step.html */
	};

	run Solve()
}
